/*
 **** PREGUNTAR SI TIENEN DUDAS CON ALGUN PUNTO MENCIONADO ****

  1. Sean lo mas explicito posible y apliquen todo lo que vean conveniente en todo lo que encuentren.
  2. Puntos adiciones:
      +1 = Si se aplica mas cosas de las que se indican
      +1 = Encontrar la solucion en el caso de clonacion de variable

*/
// Uso de Let y Const
var nombre = "Tony Stark";
var edad = 51;
var PERSONAJE = {
    nombre: nombre,
    edad: edad
};
var batman;
batman = {
    nombre: "Bruno Díaz",
    artesMarciales: ["Karate", "Aikido", "Wing Chun", "Jiu-Jitsu"]
};
// Convertir esta funcion a una funcion de flecha
var resultadoDoble = function (a, b) {
    return (a + b) * 2;
};
// Función con parametros obligatorios, opcionales y por defecto
// donde NOMBRE = obligatorio
//       PODER  = opcional
//       ARMA   = por defecto = "arco"
function getSuperHeroe(nombre, poder, arma) {
    if (arma === void 0) { arma = "arco"; }
    var mensaje;
    if (poder) {
        mensaje = nombre + " tiene el poder de: " + poder + " y un arma: " + arma;
    }
    else {
        mensaje = nombre + " tiene un " + poder;
    }
}
;
// Cree una clase que permita manejar la siguiente estructura
// La clase se debe de llamar rectangulo,
// debe de tener dos propiedades:
//   * base
//   * altura
// También un método que calcule el área  =  base * altura,
// ese método debe de retornar un numero.
var IRectangulo = /** @class */ (function () {
    function IRectangulo(base, altura) {
        this.base = base;
        this.altura = altura;
    }
    IRectangulo.prototype.getCalculo = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            setTimeout(function () {
                _this.area = _this.altura + _this.base;
                return resolve(_this.area);
            }, 2000);
        });
    };
    return IRectangulo;
}());
var calculo = new IRectangulo(10, 10);
calculo.getCalculo().then(function (info) {
    console.log(info);
}, function (err) {
    console.log(err, 'Error');
});
