/* 
 **** PREGUNTAR SI TIENEN DUDAS CON ALGUN PUNTO MENCIONADO ****

  1. Sean lo mas explicito posible y apliquen todo lo que vean conveniente en todo lo que encuentren.
  2. Puntos adiciones: 
      +1 = Si se aplica mas cosas de las que se indican
      +1 = Encontrar la solucion en el caso de clonacion de variable

*/

// Uso de Let y Const

var nombre = "Tony Stark";
var edad = 51;

var PERSONAJE = {
  nombre: nombre,
  edad: edad
};


// Cree una interfaz que sirva para validar el siguiente objeto

interface IHeroe{
  nombre:string;
  artesMarciales:string[];
}

let batman:IHeroe;
 batman = {
  nombre: "Bruno Díaz",
  artesMarciales: ["Karate","Aikido","Wing Chun","Jiu-Jitsu"]
}


// Convertir esta funcion a una funcion de flecha


const resultadoDoble =( a:number, b:number) =>{
  return (a + b) * 2
}


// Función con parametros obligatorios, opcionales y por defecto
// donde NOMBRE = obligatorio
//       PODER  = opcional
//       ARMA   = por defecto = "arco"
function getSuperHeroe( 
  nombre:string, 
  poder?:string, 
  arma:string = "arco" ){

  var mensaje;
  if( poder ){
    mensaje = nombre + " tiene el poder de: " + poder + " y un arma: " + arma;
  }else{
    mensaje = nombre + " tiene un " + poder;
  }
};

// Cree una clase que permita manejar la siguiente estructura
// La clase se debe de llamar rectangulo,
// debe de tener dos propiedades:
//   * base
//   * altura
// También un método que calcule el área  =  base * altura,
// ese método debe de retornar un numero.

class IRectangulo {
base :number;
altura : number;
area:number;

constructor(base: number, altura: number){
  this.base = base;
  this.altura = altura;
}

getCalculo() {
  return new Promise <number>( (resolve, reject) => {
    setTimeout( () => {
        this.area = this.altura + this.base
      return resolve(this.area);
    }, 2000);
  });
}

}

const calculo: IRectangulo = new IRectangulo(10,10);

calculo.getCalculo().then(
  (info: number) => {
    console.log(info);
  },
  (err:any) => {
    console.log(err, 'Error');
  },
)





